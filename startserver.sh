#!/usr/bin/env bash

set -euo pipefail

_esc_var() {
    printf '%s\n' "$@" | sed -e 's/[\/&]/\\&/g'
}

# copy the worldmods
if [[ ! -d /var/lib/minetest/worlds/world ]]; then
    mkdir -p /var/lib/minetest/worlds/world/worldmods
fi
pushd /usr/local/share/minetest/games/unej/worldmods
rsync -avhW --delete --no-compress --filter=". filter_rules.txt" * /var/lib/minetest/worlds/world/worldmods/
popd

# set the database info
# if the world.mt file exists and if the backend is postgres
# i.e. PG_HOST is set
world_mt_file="/var/lib/minetest/worlds/world/world.mt"
if [[ -f "$world_mt_file" ]]; then
    if $(grep -q -E "^gameid\s*=\s*.*$" "$world_mt_file"); then
        sed -i "s/^gameid[ ]*=[ ]*.*$/gameid = unej/g" $world_mt_file
    else
        echo "gameid = unej" >> $world_mt_file
    fi
fi

if [[ -f "$world_mt_file" ]] && \
  [[ -n "${PG_HOST:-}" ]]; then
    # To allow migration we don't change the various backends.
    # backend = sqlite3
    # player_backend = sqlite3
    # auth_backend = sqlite3
    pgsql_connection_str="host=${PG_HOST} port=${PG_PORT:-5432} user=${PG_USER} dbname=${PG_DB}"
    if $(grep -q -E "^pgsql_connection\s*=\s*.*$" $world_mt_file); then
        sed -i "s/^pgsql_connection[ ]*=[ ]*.*$/pgsql_connection = $(_esc_var "${pgsql_connection_str}")/g" $world_mt_file
    else
        echo "pgsql_connection = ${pgsql_connection_str}" >> $world_mt_file
    fi

    if $(grep -q -E "^pgsql_player_connection\s*=\s*.*$" $world_mt_file); then
        sed -i "s/^pgsql_player_connection[ ]*=[ ]*.*$/pgsql_player_connection = $(_esc_var "${pgsql_connection_str}")/g" $world_mt_file
    else
        echo "pgsql_player_connection = ${pgsql_connection_str}" >> $world_mt_file
    fi

    if $(grep -q -E "^pgsql_auth_connection\s*=\s*.*$" $world_mt_file); then
        sed -i "s/^pgsql_auth_connection[ ]*=[ ]*.*$/pgsql_auth_connection = $(_esc_var "${pgsql_connection_str}")/g" $world_mt_file
    else
        echo "pgsql_auth_connection = ${pgsql_connection_str}" >> $world_mt_file
    fi

    if $(grep -q -E "^pgsql_mapserver_connection\s*=\s*.*$" $world_mt_file); then
        sed -i "s/^pgsql_mapserver_connection[ ]*=[ ]*.*$/pgsql_mapserver_connection = $(_esc_var "${pgsql_connection_str}")/g" $world_mt_file
    else
        echo "pgsql_mapserver_connection = ${pgsql_connection_str}" >> $world_mt_file
    fi

    if $(grep -q -E "^pgsql_mod_storage_connection\s*=\s*.*$" $world_mt_file); then
        sed -i "s/^pgsql_mod_storage_connection[ ]*=[ ]*.*$/pgsql_mod_storage_connection = $(_esc_var "${pgsql_connection_str}")/g" $world_mt_file
    else
        echo "pgsql_mod_storage_connection = ${pgsql_connection_str}" >> $world_mt_file
    fi

fi

minetest_conf_file="/etc/minetest/minetest.conf"

if [[ ! -f "$minetest_conf_file" ]]; then
    cp /usr/local/share/doc/minetest/minetest.conf.example "$minetest_conf_file"
fi

if [[ -n "$MAPSERVER_URL" ]]; then
    if $(grep -q -E "^mapserver\.url\s*=\s*.*$" $minetest_conf_file); then
        sed -i "s/^mapserver\.url[ ]*=[ ]*.*$/mapserver.url = $(_esc_var "${MAPSERVER_URL}")/g" $minetest_conf_file
    else
        echo "mapserver.url = $MAPSERVER_URL" >> $minetest_conf_file
    fi
fi

if [[ -n "$MAPSERVER_KEY" ]]; then
    if $(grep -q -E "^mapserver\.key\s*=\s*.*$" $minetest_conf_file); then
        sed -i "s/^mapserver\.key[ ]*=[ ]*.*$/mapserver.key = $(_esc_var "${MAPSERVER_KEY}")/g" $minetest_conf_file
    else
        echo "mapserver.key = $MAPSERVER_KEY" >> $minetest_conf_file
    fi
fi


exec minetestserver \
    ${CLI_ARGS:-} \
    --config "/etc/minetest/minetest.conf" \
    --logfile "/var/log/minetest/debug.txt" \
    --world "/var/lib/minetest/worlds/world" \
    --gameid unej
    --port 30000
