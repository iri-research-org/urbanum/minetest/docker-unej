#!/usr/bin/env bash
set -Eeuo pipefail

# check arguments for the option to create config file
# return true if there is one
_mapserver_create_env() {
    local arg
    for arg; do
        case "$arg" in
            -createconfig)
                return 0
                ;;
        esac
    done
    return 1
}

_bool(){ if [[ "$1" == "1" ]] || [[ "$1" == "true" ]]; then return 0; else return 1; fi }

if [[ "$1" =~ mapserver$ ]] && _mapserver_create_env "$@"; then

    $("$@")
    ## replace the value of serverkey in config file
    if [[ -n "${MAPSERVER_KEY:-}" ]]; then
        jq --arg secretkey "${MAPSERVER_KEY}" '.webapi.secretkey = $secretkey' mapserver.json | sponge mapserver.json
    fi
    if _bool "${MAPSERVER_PROMETHEUS_ENABLED:-}"; then
        jq '.enableprometheus = true' mapserver.json | sponge mapserver.json
    else
        jq '.enableprometheus = false' mapserver.json | sponge mapserver.json
    fi
    exit 0;
fi


if [[ "$1" =~ mapserver$ ]] && ! _mapserver_create_env "$@" && [[ -n "${MAPSERVER_KEY:-}" ]]; then
    ## if config file not exists create it
    if [[ ! -f mapserver.json ]]; then
        /bin/mapserver -createconfig
    fi
    ## replace the value of serverkey in config file
    jq --arg secretkey "${MAPSERVER_KEY}" '.webapi.secretkey = $secretkey' mapserver.json | sponge mapserver.json
fi

if [[ "$1" =~ mapserver$ ]] && ! _mapserver_create_env "$@" && _bool "${MAPSERVER_PROMETHEUS_ENABLED:-}"; then
    ## if config file not exists create it
    if [[ ! -f mapserver.json ]]; then
        /bin/mapserver -createconfig
    fi
    jq '.enableprometheus = true' mapserver.json | sponge mapserver.json
fi

exec "$@"