import bpy
import sys

filepath = sys.argv[(sys.argv.index("--output") + 1)]

bpy.ops.export_scene.gltf(export_format='GLB', export_materials='EXPORT', filepath=filepath, check_existing=False)
